import React from 'react';

import 'bootstrap/dist/css/bootstrap.min.css'

import { PokemonContainer } from './catch-them-all/components/PokemonContainer'

function App() {
  return (
    <div className="App">
      <PokemonContainer />
    </div>
  )
}

export default App;
