// empty list = “zero” state on the app like “No pokemon captured yet”
import React from 'react'
import { PokemonItem } from './PokemonItem'

export const PokemonList = ({ list }) => {
  const pokemonList =
    list.length > 0
      ? list.map((pokemon) => {
          const id = pokemon.url.split('/')[6]
          return <PokemonItem key={id} {...pokemon} id={id} />
        })
      : 'No pokemon captured yet'
  return pokemonList
}
