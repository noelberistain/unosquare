import React from 'react'
import { Media } from 'react-bootstrap'

export const PokemonItem = ({ name, id }) => {
  return (
    <Media>
      <img
        src="https://dummyimage.com/150x150/808080/ffffff.jpg&text=GenericPokemon"
        alt={`visual representation of ${name}`}
        style={{ margin: '0 2rem 1rem 0' }}
      />

      <Media.Body>
        <h6>PokemonID</h6>
        {id}
        <h6>PokemonName</h6>
        {name}
      </Media.Body>
    </Media>
  )
}
