import React, { useEffect, useRef, useState } from 'react'
import { Container, Row } from 'react-bootstrap'
import { PokemonList } from './PokemonList'

export const url = 'https://pokeapi.co/api/v2/pokemon/?limit=11&offset=0'

export const PokemonContainer = () => {
  const inputRef = useRef(null)

  const [list, setList] = useState([])
  let [localId, setLocalId] = useState(12)

  useEffect(() => {
    fetch(url)
      .then((data) => data.json())
      .then(({ results }) => {
        setList(results)
      })
      .catch((e) => {
        console.log(`Error = ${e}`)
      })
  }, [])

  const createPokemon = () => {
      const newPokemon = {
        url: `https://pokeapi.co/api/v2/pokemon/${localId}`,
        name: inputRef.current.value
      }
      setList([...list, newPokemon])
  }

  const handleClick = (e) => {
    e.preventDefault()
    if (inputRef.current.value.length > 0) {
      createPokemon()
      setLocalId(localId + 1)
      inputRef.current.value = ''
    }
  }

  return (
    <Container fluid="md">
      <Row style={{ justifyContent: 'center', margin: '3rem' }}>
        <nav>
          <input type="text" ref={inputRef} />
          <button onClick={handleClick}>Catch!</button>
        </nav>
      </Row>
      <Row md={2} style={{ alignContent: 'center' }}>
        <PokemonList list={list} />
      </Row>
    </Container>
  )
}
